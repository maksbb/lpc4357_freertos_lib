################################################################################
# Build the core FreeRTOS as a static lib
################################################################################

TRGT = arm-none-eabi-
CC   = $(TRGT)gcc
CP   = $(TRGT)objcopy
BIN  = $(CP) -O ihex
AR   = $(TRGT)ar

MCU = cortex-m4
FPU = -mfloat-abi=softfp -mfpu=fpv4-sp-d16 -D__FPU_USED=1

MCFLAGS = -mthumb -mcpu=$(MCU) $(FPU)

# Define optimisation level here
OPT = -Os

CFLAGS=-DDEBUG -D__CODE_RED -D__USE_LPCOPEN -DCORE_M4 -D__MULTICORE_NONE -D__NEWLIB__ \
  -I"inc" -I"../lpc_chip_43xx/inc" -I"../lpc_board_ea_oem_4357/inc" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -fsingle-precision-constant -mcpu=cortex-m4 -mfpu=fpv4-sp-d16 -mfloat-abi=softfp -mthumb -D__NEWLIB__ -specs=redlib.specs -MMD -MP
LDFLAGS=-lrt



# Add inputs and outputs from these tool invocations to the build variables
SOURCES = \
src/croutine.c \
src/FreeRTOSCommonHooks.c \
src/event_groups.c \
src/heap_2.c \
src/list.c \
src/port.c \
src/queue.c \
src/tasks.c \
src/timers.c

OBJECTS=$(SOURCES:.c=.o)

FREERTOSLIB=libfreertos_lpc4357.a

all: $(SOURCES) $(FREERTOSLIB)

$(FREERTOSLIB): $(OBJECTS)
	$(AR) $(ARFLAGS) $(FREERTOSLIB)  $?

.c.o:
	$(CC) $(CFLAGS) $< -o $@

clean:
	rm -f src/*.o $(FREERTOSLIB)


