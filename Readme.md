
## Build instructions

set your path to point to the gcc toolchain within LPCXpresso,
eg.
```
export PATH=/usr/local/lpcxpresso_7.5.0_254/lpcxpresso/tools/bin:$PATH
```

### Dependancies

The build currently has hard-coded dependency on having the lpc_chip_43xx and lpc_board_ea_oem_4357 repos in the parent folder.


### Notes

You can strip down the built lib with:
```
arm-none-eabi-strip  libfreertos_lpc4357.a
```
